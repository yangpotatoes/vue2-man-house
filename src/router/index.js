import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'
import Login from '../layouts/Login.vue'
import Layout from '../layouts/Layout.vue'
Vue.use(VueRouter);


const routes = [
  {
    path: '/',
    name: 'Layout',
    component: Layout,
    redirect: '/workingfill',
    meta: {
      requireAuth: true
    },
    children: [
      {
        path: '/workingfill',
        name: 'workingfill',
        component: () => import('../views/Workingfill.vue'),
        meta: {
          requireAuth: true
        },
      },
      {
        path: '/workingsee',
        name: 'workingsee',
        component: () => import('../views/WorkingSee.vue'),
        meta: {
          requireAuth: true
        },
      },
      {
        path: '/workingreview',
        name: 'workingreview',
        component: () => import('../views/WorkingReview.vue'),
        meta: {
          requireAuth: true
        },
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to,from,next) => {
  if(to?.meta.requireAuth){
    if(localStorage.getItem('token') !== ''){
      next()
    }else{
      next({path: '/login'})
    }
  }else{
    next()
  }
})
export default router
