const loginModule = {
    state:{
        token: ''
    },
    getters: {
        getToken (state, getters) {
            console.log('getter', state, getters, 'getToken');
            return state.token;
        }
    },
    mutations: {
        setToken(state,payload) {
            console.log(state,'state',payload,'payload','token');
            state.token = payload;
          }
    },
    actions: {}
};
export default loginModule;