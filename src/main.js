import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import './assets/css/global.scss';
// import axios from 'axios';
// axios.defaults.baseURL = 'http://192.168.6.23:3005'
import http from './http/index';
Vue.use(ElementUI);
Vue.prototype.$http = http;
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
