const express = require('express');
const workFillController = require('../controlers/workfill.controler');
const router = express.Router();

router.post('/',workFillController.createOne);
// 工时信息查询
router.get('/',workFillController.findOne);
router.delete('/',workFillController.delOne);
router.put('/',workFillController.putOne);
module.exports = router;