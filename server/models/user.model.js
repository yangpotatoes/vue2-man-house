// 定义sequelize的model
module.exports = (sequelize, Sequelize) => {
  // sequelize 是我们定义的orm模型实例，Sequelize是库所提供的orm类
  const User = sequelize.define('user', {
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    },
    pwd: {
      type: Sequelize.STRING
    },
    email: {
      type: Sequelize.STRING
    },
    time: {
      type: Sequelize.DATE,
    },
    avator: {
      type: Sequelize.STRING
    }
  })
  return User;
}