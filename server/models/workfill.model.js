// 定义工时填报的model
module.exports = (sequelize, Sequelize) => {
    // sequelize 是我们定义的orm模型实例，Sequelize是库所提供的orm类
    const workFill = sequelize.define('workfill', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      type: {
        type: Sequelize.STRING
      },
      project: {
        type: Sequelize.STRING
      },
      branch: {
        type: Sequelize.STRING
      },
      task: {
        type: Sequelize.STRING,
      },
      hours: {
        type: Sequelize.STRING
      },
      content: {
        type: Sequelize.STRING
      },
      date: {
        type: Sequelize.STRING
      }
    })
    return workFill;
  }