const db = require('../models');
const workFill = db.workFill;
// 创建
exports.createOne = async (req,res) => {
    console.log(req.body.data,'KKKKKKKKKKKKKKKKKKKK');
    const fill = await workFill.create(req?.body?.data);
    fill?.id ? '' : fill.id = Math.random() * 10000;
    console.log('fill',fill);
    if(fill.uniqno === 1){
        res.send({
            msg: '记录新建成功',
            code: 200
        })
    }else{
        res.status(500).send({
            msg: '记录新建失败',
        })
    }
};
// 查找
exports.findOne = async (req,res) => {
    const search = await workFill.findAll();
    // console.log(search,'查询到的数据');
    if(search.length) {
        const data = search?.map((it,index) => {
            console.log(it?.dataValues)
            return Object.assign(it?.dataValues,{check: index ? true : false,checkper: '魂魂张'})
        })
        res.status(200).send({
            msg: '查询成功',
            data: data
        });
    }else{
        res.status(203).send({
            msg: '没有数据'
        })
    }
};
// 删除
exports.delOne = async (req,res) => {
    const {id} = req.body;
    console.log(req.body,'删除元组的id');
    const hasOne = await workFill.findOne({
        where: {
            id: id
        }
    });
    const con = await hasOne.destroy();
    console.log(con,'lll')
    if(con.uniqno === 1){
        res.status(200).send({
            msg: '记录删除成功'
        })
    }else{
        res.status(500).send({
            msg: '记录删除失败'
        })
    }
};
// 更新
exports.putOne = async (req,res) => {
    // console.log(req.body.data);
    let fill = await workFill.findOne({
        where:{
            id: req?.body?.data?.id
        }
    });
    await fill.update(req?.body?.data);
    const con = await fill.save();
    // console.log('fillssssssssssssssss',con);
    if(con.uniqno === 1){
        res.send({
            msg: '记录更新成功',
            code: 200
        })
    }else{
        res.status(500).send({
            msg: '记录更新失败',
        })
    }
};