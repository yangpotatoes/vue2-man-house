﻿# Host: localhost  (Version: 5.7.26)
# Date: 2022-10-08 08:26:11
# Generator: MySQL-Front 5.3  (Build 4.234)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "users"
#

CREATE TABLE `users` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pwd` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `avator` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "users"
#

INSERT INTO `users` VALUES ('10001','魂魂张','123456','ben_xiaokang@126.com',NULL,NULL,'1899-12-29 00:00:00','1899-12-29 00:00:00');

#
# Structure for table "workfills"
#

CREATE TABLE `workfills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `project` varchar(255) DEFAULT NULL,
  `branch` varchar(255) DEFAULT NULL,
  `task` varchar(255) DEFAULT NULL,
  `hours` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52226 DEFAULT CHARSET=utf8;

#
# Data for table "workfills"
#

INSERT INTO `workfills` VALUES (52224,'项目工时','工时系统软件开发项目','b001','r002','5','工时增删改查的数据库和后端接口完成；','2022-10-05','2022-10-04 04:30:02','2022-10-07 06:26:25');
