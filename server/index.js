const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
const corsOptions = {
  origin: "*"
}
app.use(cors(corsOptions));
// content-type:application/json
app.use(bodyParser.json());
// content-type：application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// 登录router
app.use('/login', require('./routes/loginRouter.js'));
// 工时填报router
app.use('/workfill',require('./routes/workfillRouter.js'))
// 原生解决跨域问题
// app.all("*", function (req, res, next) {
//   // 设置允许跨域的域名,*代表允许任意域名跨域
//   res.header('Access-Control-Allow-Origin', '*');
//   // 允许的header类型
//   res.header('Access-Control-Allow-Headers', 'content-type');
//   // 跨域允许的请求方式
//   res.header('Access-Control-Allow-Methods', 'DELETE,PUT,POST,GET,OPTIONS');
//   if (req.method.toLowerCase() == 'options')
//     res.send(200); // 让options 尝试请求快速结束
//   else
//     next();
// })
app.listen(3005, () => {
  console.log('服务器启动于:localhost:3005')
});
// 开启使用数据库
const db = require('./models/index.js');
db.sequelize.sync();